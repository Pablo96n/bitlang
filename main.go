package main

import (
	bit "bitlogic-language/bitlang"
	"fmt"
	"os"
	str "strings"
)

func main() {
	args := getParsedArgs()

	if args.Command == help {
		printHelp()
		os.Exit(0)
	}

	if args.Command == dbgprint {
		// TODO: print tokens
		text := "proc main (args: string) : int {\n\tvar argc: int = len(args)\n\n\tprint(\"hello world! I've been given {} arguments\", argc)\n\n\treturn 0\n}"
		fileName := "my_file.bit"
		errors := []bit.Error{}
		lexer := bit.NewLexer()
		tokens := lexer.Lex(text, &errors)
		if len(errors) > 0 {
			for _, err := range errors {
				bit.PrintError(fileName, text, err)
			}
			os.Exit(1)
		}
		bit.Print(tokens)
		return
	}

	if args.Command == run {
		// TODO: open file and run it
		// TODO: print errors if found
	} else {
		if len(args.CommandStr) != 0 {
			fmt.Println("Unknown command", args.CommandStr)
		} else {
			fmt.Println("No command passed")
		}
		printHelp()
		os.Exit(1)
	}
}

func printHelp() {
	fmt.Println("Usage:")
	fmt.Println("\tbitlang [command] <args>")
	fmt.Println()
	fmt.Println("Commands:")
	fmt.Println("\thelp             prints this message.")
	fmt.Println("\trun <file>       runs the a file.")
	fmt.Println("\tdbgprint <file>  lexes a file and print the tokens.")
}

type command int

const (
	unknownCommand command = iota
	run            command = iota
	help           command = iota
	dbgprint       command = iota
)

type parsedArgs struct {
	Command    command
	CommandStr string
	Arguments  []string
}

func getParsedArgs() parsedArgs {
	args := os.Args[1:] // without the program path
	if len(args) == 0 {
		return parsedArgs{
			Command: unknownCommand,
		}
	}

	command := args[0]
	_parsedArgs := parsedArgs{
		CommandStr: command,
		Arguments:  args[1:],
	}

	switch str.ToLower(command) {
	case "run":
		_parsedArgs.Command = run
	case "help":
		_parsedArgs.Command = help
	case "dbgprint":
		_parsedArgs.Command = dbgprint
	default:
		_parsedArgs.Command = unknownCommand
	}

	return _parsedArgs
}
