package bitlang

import (
  "fmt"
)

type Logger interface {
  Printf(fmtStr string, v ...interface{})
  Println(fmtStr string)
  Print(fmtStr string)
}

var Log Logger = &LoggerImpl{}

type LoggerImpl struct {
  Logger
}

func (log *LoggerImpl) Printf(fmtStr string, v ...interface{}) {
  fmt.Printf(fmtStr, v...)
}

func (log *LoggerImpl) Println(fmtStr string) {
  fmt.Println(fmtStr)
}

func (log *LoggerImpl) Print(fmtStr string) {
  fmt.Print(fmtStr)
}

type Span struct {
  start uint
  end uint
}

type Error struct {
  errType   errorType
  message   string
  span      Span
  hint      string
  hint_span Span
}

func ErrorMessage(message string, span Span) Error {
  err := Error{}

  err.errType = errorTypeMessage
  err.message =  message
  err.span =  span
  
  return err
}

func ErrorMessageWithHint(message string, span Span, hint string, hint_span Span) Error {
  return Error{
    errorTypeMessageWithHint,
    message,
    span,
    hint,
    hint_span,
  }
}

func PrintError(fileName string, fileContents string, err Error) {
  switch err.errType {
  case errorTypeMessage:
    displayMessageWithSpan(errorMsgSeverityError, fileName, fileContents, err.message, err.span)
  case errorTypeMessageWithHint: 
    displayMessageWithSpan(errorMsgSeverityError, fileName, fileContents, err.message, err.span)
    displayMessageWithSpan(errorMsgSeverityHint, fileName, fileContents, err.hint, err.hint_span)
  }
}

type errorMsgSeverity int
const (
  errorMsgSeverityHint  errorMsgSeverity = iota
  errorMsgSeverityError errorMsgSeverity = iota
)

type errorType int
const (
  errorTypeMessage         errorType = iota
  errorTypeMessageWithHint errorType = iota
)

func severityName(severity errorMsgSeverity) string {
  switch severity {
  case errorMsgSeverityHint:
    return "Hint"
  case errorMsgSeverityError:
    return "Error"
  }
  panic("Unknown severity")
}

func severityAnsiColorCode(severity errorMsgSeverity) string {
  switch severity {
  case errorMsgSeverityHint:
    return "94"  // Bright Blue
  case errorMsgSeverityError:
    return "31" // Red
  }
  panic("Unknown severity")
}

func displayMessageWithSpan(severity errorMsgSeverity, file_name string, fileContents string, message string, span Span) {
  Log.Printf("%s: %s\n", severityName(severity), message)

  line_spans := gatherLineSpans(fileContents)

  line_index := uint(1)
  largest_line_number := uint(len(line_spans))

  width := uint(len(fmt.Sprintf("%d", largest_line_number)))

  for line_index < largest_line_number {
      if span.start >= line_spans[line_index].start && span.start <= line_spans[line_index].end {
          column_index := span.start - line_spans[line_index].start

          Log.Printf("----- \u001b[33m%s:%d:%d\u001b[0m\n", file_name, line_index + 1, column_index + 1)

          if line_index > 0 {
              printSourceLine(severity, fileContents, line_spans[line_index - 1], span, line_index, largest_line_number)
          }

          printSourceLine(severity, fileContents, line_spans[line_index], span, line_index + 1, largest_line_number)

          for x := uint(0); x <= (span.start - line_spans[line_index].start + width + 4); x++ {
            Log.Print(" ")
          }

          Log.Printf("\u001b[%sm^- %s\u001b[0m\n", severityAnsiColorCode(severity), message)

          for line_index < largest_line_number && span.end > line_spans[line_index].start {
              line_index++
              if line_index >= largest_line_number {
                  break
              }

              printSourceLine(severity, fileContents, line_spans[line_index], span, line_index + 1, largest_line_number)

              break
          }
      } else {
          line_index++
      }

  }
  Log.Println("\u001b[0m-----")
}

func printSourceLine(severity errorMsgSeverity, fileContents string, file_span Span, error_span Span, line_number uint, largest_line_number uint) {
  index := file_span.start

  Log.Printf(" %d | ", line_number)

  for index <= file_span.end {
      c := ' '
      if index < file_span.end {
          c = rune(fileContents[index])
      } else if error_span.start == error_span.end && index == error_span.start {
          c = '_'
      }

      if (index >= error_span.start && index < error_span.end) || (error_span.start == error_span.end && index == error_span.start) {
        Log.Printf("\u001b[%sm%c", severityAnsiColorCode(severity), c)
      } else {
        Log.Printf("\u001b[0m%c", c)
      }

      index++
  }
  Log.Print("\n")
}

func gatherLineSpans(fileContents string) []Span {
  idx := uint(0)
  output := []Span{}

  start := idx
  for idx < uint(len(fileContents)) {
    if fileContents[idx] == '\n' {
      output = append(output, Span{start, idx})
      start = idx + 1
    }
    idx += 1
  }
  if start < idx {
    output = append(output, Span{start, idx})
  }

  return output
}
