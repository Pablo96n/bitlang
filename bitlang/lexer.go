package bitlang

import (
	"fmt"
)

//go:generate go run golang.org/x/tools/cmd/stringer -type=TokenKind
type TokenKind int

const (
	TokenEOF        TokenKind = iota
	TokenIdentifier TokenKind = iota
	TokenNumber     TokenKind = iota
	TokenString     TokenKind = iota

	// Others
	TokenEOL    TokenKind = iota
	TokenColon  TokenKind = iota
	TokenCurlyL TokenKind = iota
	TokenCurlyR TokenKind = iota
	TokenParenL TokenKind = iota
	TokenParenR TokenKind = iota
	TokenAssign TokenKind = iota

	// Operators

	// Keywords
	TokenProcedure TokenKind = iota
	TokenVar       TokenKind = iota
	TokenReturn    TokenKind = iota
	TokenIf        TokenKind = iota
	TokenElse      TokenKind = iota
	TokenLoop      TokenKind = iota
	TokenBreak     TokenKind = iota
	TokenContinue  TokenKind = iota
	TokenInt       TokenKind = iota
	TokenFloat     TokenKind = iota
	TokenBool      TokenKind = iota
	TokenTrue      TokenKind = iota
	TokenFalse     TokenKind = iota
)

type Token struct {
	Kind  TokenKind
	Value string
}

type Lexer interface {
	Lex(text string, errors *[]Error) []Token
}

func NewLexer() Lexer {
	return &lexerImpl{}
}

type lexerState int

const (
	stateStart      lexerState = iota
	stateIdentifier lexerState = iota
	stateNumber     lexerState = iota
	stateString     lexerState = iota
	stateFailed     lexerState = iota
)

var keywords = map[string]TokenKind{
	"proc":     TokenProcedure,
	"var":      TokenVar,
	"return":   TokenReturn,
	"if":       TokenIf,
	"else":     TokenElse,
	"loop":     TokenLoop,
	"break":    TokenBreak,
	"continue": TokenContinue,
	"int":      TokenInt,
	"float":    TokenInt,
	"bool":     TokenBool,
	"true":     TokenTrue,
	"false":    TokenFalse,
}

var commons = map[rune]TokenKind{
	'\n': TokenEOL,
	':':  TokenColon,
	'{':  TokenCurlyL,
	'}':  TokenCurlyR,
	'(':  TokenParenL,
	')':  TokenParenR,
	'=':  TokenAssign,
}

type lexerImpl struct {
	Lexer
	errors          *[]Error
	tokens          []Token
	currentState    lexerState
	currentToken    *Token
	currentIndex    uint
	text            []rune
	valueIndexStart uint
	valueIndexEnd   uint
	lineStart       uint
	lineEnd         uint
}

func (lexer *lexerImpl) Lex(text string, errors *[]Error) []Token {
	lexer.tokens = []Token{}
	if len(text) <= 0 {
		return lexer.tokens
	}

	lexer.errors = errors
	lexer.currentState = stateStart
	lexer.valueIndexStart = 0
	lexer.valueIndexEnd = 0
	lexer.currentIndex = 0
	lexer.text = []rune(text)

	for lexer.currentIndex = 0; lexer.currentIndex < uint(len(lexer.text)); {
		runeChar := lexer.text[lexer.currentIndex]
		var incrementIndex bool
		switch lexer.currentState {
		case stateStart:
			incrementIndex = lexer.startState(runeChar)
		case stateNumber:
			incrementIndex = lexer.numberState(runeChar)
		case stateIdentifier:
			incrementIndex = lexer.identifierState(runeChar)
		case stateString:
			incrementIndex = lexer.stringState(runeChar)
		case stateFailed:
			// TODO: if failed then the interpreter wont be able to run the program
			// so we should print errors and exit.
			return []Token{}
		default:
			errorMsg := fmt.Sprintf("Unknown lexerState %d", lexer.currentState)
			panic(errorMsg)
		}

		if incrementIndex {
			lexer.currentIndex++
		}
	}

	// Add EOF token at the end of the file
	if lexer.currentState == stateStart {
		lexer.currentToken = &Token{}
		lexer.currentToken.Kind = TokenEOF
		lexer.currentToken.Value = ""
		lexer.addToken()
	} else {
		panic("lexerState at the end should always be \"stateStart\"")
	}

	return lexer.tokens
}

func (lexer *lexerImpl) startState(runeChar rune) bool {
	if isWhiteSpace(runeChar) {
		return true
	}

	index := lexer.currentIndex
	if isIdentifierStartChar(runeChar) {
		lexer.valueIndexStart = uint(index)
		lexer.valueIndexEnd = uint(index)
		lexer.currentToken = &Token{}
		lexer.currentToken.Kind = TokenIdentifier

		lexer.currentState = stateIdentifier
	} else if isDigitChar(runeChar) {
		lexer.valueIndexStart = uint(index)
		lexer.valueIndexEnd = uint(index)
		lexer.currentToken = &Token{}
		lexer.currentToken.Kind = TokenNumber

		lexer.currentState = stateNumber
	} else if runeChar == '"' {
		lexer.valueIndexStart = uint(index)
		lexer.valueIndexEnd = uint(index)
		lexer.currentToken = &Token{}
		lexer.currentToken.Kind = TokenString

		lexer.currentState = stateString
	} else if kind, found := commons[runeChar]; found {
		lexer.valueIndexStart = uint(index)
		lexer.valueIndexEnd = uint(index + 1)
		lexer.currentToken = &Token{}
		lexer.currentToken.Kind = kind
		lexer.currentToken.Value = lexer.getValue()
		lexer.addToken()
	} else {
		msg := fmt.Sprintf("Invalid character '%c'", runeChar)
		lexer.setError(msg, Span{lexer.lineStart, lexer.lineEnd})
		lexer.currentState = stateFailed
	}

	return true
}

func (lexer *lexerImpl) numberState(runeChar rune) bool {
	lexer.valueIndexEnd = uint(lexer.currentIndex)

	if !isTokenRune(runeChar) {
		lexer.setError("Invalid number", Span{lexer.lineStart, lexer.lineEnd})
		lexer.currentState = stateFailed
	} else if !isDigitChar(runeChar) {
		lexer.currentState = stateStart
		lexer.currentToken.Value = lexer.getValue()
		lexer.addToken()

		// We only increment the index if it is a whitespace
		// else we need to reprocess that runeChar
		return isWhiteSpace(runeChar)
	}

	return true
}

func (lexer *lexerImpl) identifierState(runeChar rune) bool {
	lexer.valueIndexEnd = uint(lexer.currentIndex)

	if !isTokenRune(runeChar) {
		msg := fmt.Sprintf("Invalid identifier name: bad char '%c'", runeChar)
		lexer.setError(msg, Span{lexer.lineStart, lexer.lineEnd})
		lexer.currentState = stateFailed
	} else if !isIdentifierChar(runeChar) {
		lexer.currentState = stateStart
		lexer.currentToken.Value = lexer.getValue()

		tokenType, is_keyword := keywords[lexer.currentToken.Value]
		if is_keyword {
			lexer.currentToken.Kind = tokenType
		}

		lexer.addToken()

		// We only increment the index if it is a whitespace
		// else we need to reprocess that runeChar
		return isWhiteSpace(runeChar)
	}
	return true
}

func (lexer *lexerImpl) stringState(runeChar rune) bool {
	if runeChar == '"' {
		lexer.currentIndex += 1 //include the '"' in the token value
		lexer.valueIndexEnd = uint(lexer.currentIndex)
		lexer.currentToken.Value = lexer.getValue()
		lexer.addToken()

		lexer.currentState = stateStart
	}

	// TODO: Handle escaped chars
	return true
}

func (lexer *lexerImpl) addToken() {
	lexer.tokens = append(lexer.tokens, *lexer.currentToken)
	lexer.currentToken = nil
}

func (lexer *lexerImpl) getValue() string {
	return string(lexer.text[lexer.valueIndexStart:lexer.valueIndexEnd])
}

func (lexer *lexerImpl) setError(message string, span Span) {
	err := ErrorMessage(message, span)
	*lexer.errors = append(*lexer.errors, err)
}

/**
 * This does not count '\n' since we make a token out of it.
 */
func isWhiteSpace(runeChar rune) bool {
	switch runeChar {
	case /*'\n', */ '\r', '\t', '\v', '\f', ' ':
		return true
	default:
		return false
	}
}

func isDigitChar(runeChar rune) bool {
	// [0-9]
	if runeChar >= 48 && runeChar <= 57 {
		return true
	}

	return false
}

func isIdentifierStartChar(runeChar rune) bool {
	// [A-Z]
	if runeChar >= 65 && runeChar <= 90 {
		return true
	}

	// [a-z]
	if runeChar >= 97 && runeChar <= 122 {
		return true
	}

	if runeChar == '_' {
		return true
	}

	return false
}

func isIdentifierChar(runeChar rune) bool {
	return isIdentifierStartChar(runeChar) || isDigitChar(runeChar)
}

func isTokenRune(runeChar rune) bool {
	_, found := commons[runeChar]
	return isIdentifierChar(runeChar) || found || isWhiteSpace(runeChar)
}
