package bitlang

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// Define the suite, and absorb the built-in basic suite
// functionality from testify - including a T() method which
// returns the current testing context
type LexerTests struct {
	suite.Suite
	lexer Lexer
}

// before all tests
func (suite *LexerTests) SetupSuite() {
	suite.lexer = NewLexer()
}

func (suite *LexerTests) TestLexNoWhitespaceTerminatedText() {
	/**
	 * Test if the lexer can not tokenize a token
	 *  that is not proceeded by a white space.
	 */

	assert.Panics(suite.T(), func() { suite.lexer.Lex("my_Identifier", &[]Error{}) })
	assert.Panics(suite.T(), func() { suite.lexer.Lex("0", &[]Error{}) })
}

func (suite *LexerTests) TestLexNoWhitespaceCommonTokenTerminatedIdentifier() {
	/**
	 * Test if the lexer can tokenize a token
	 *  that is proceeded by a common token.
	 */
	tokens := suite.lexer.Lex("my_Identifier:", &[]Error{})
	assert.Equal(suite.T(), 3, len(tokens))

	if len(tokens) != 3 {
		return
	}

	token := tokens[0]
	assert.Equal(suite.T(), TokenIdentifier, token.Kind)
	assert.Equal(suite.T(), "my_Identifier", token.Value)

	token = tokens[1]
	assert.Equal(suite.T(), TokenColon, token.Kind)
	assert.Equal(suite.T(), ":", token.Value)

	tokenEOF := tokens[len(tokens)-1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexNoWhitespaceCommonTokenTerminatedNumber() {
	/**
	 * Test if the lexer can tokenize a token
	 *  that is proceeded by a common token.
	 */
	tokens := suite.lexer.Lex("412583:", &[]Error{})
	assert.Equal(suite.T(), 3, len(tokens))

	if len(tokens) != 3 {
		return
	}

	token := tokens[0]
	assert.Equal(suite.T(), TokenNumber, token.Kind)
	assert.Equal(suite.T(), "412583", token.Value)

	token = tokens[1]
	assert.Equal(suite.T(), TokenColon, token.Kind)
	assert.Equal(suite.T(), ":", token.Value)

	tokenEOF := tokens[len(tokens)-1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexString() {
	/**
	 * Test if the lexer can tokenize a token
	 *  that is proceeded by a common token.
	 */
	expectedLen := 2
	tokens := suite.lexer.Lex("\"¿my/../$tr1ng!\"", &[]Error{})
	assert.Equal(suite.T(), expectedLen, len(tokens))

	if len(tokens) != expectedLen {
		return
	}

	token := tokens[0]
	assert.Equal(suite.T(), TokenString, token.Kind)
	assert.Equal(suite.T(), "\"¿my/../$tr1ng!\"", token.Value)

	tokenEOF := tokens[len(tokens)-1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexAllOtherTokens() {
	/**
	 * Test if the lexer can tokenize an Identifier
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */

	errors := []Error{}
	tokens := suite.lexer.Lex(": { } ( ) =", &errors)

	assert.Equal(suite.T(), 0, len(errors))
	assert.Equal(suite.T(), 7, len(tokens))

	if len(tokens) > 1 {
		token := tokens[0]
		assert.Equal(suite.T(), TokenColon, token.Kind)
		assert.Equal(suite.T(), ":", token.Value)

		token = tokens[1]
		assert.Equal(suite.T(), TokenCurlyL, token.Kind)
		assert.Equal(suite.T(), "{", token.Value)

		token = tokens[2]
		assert.Equal(suite.T(), TokenCurlyR, token.Kind)
		assert.Equal(suite.T(), "}", token.Value)

		token = tokens[3]
		assert.Equal(suite.T(), TokenParenL, token.Kind)
		assert.Equal(suite.T(), "(", token.Value)

		token = tokens[4]
		assert.Equal(suite.T(), TokenParenR, token.Kind)
		assert.Equal(suite.T(), ")", token.Value)

		token = tokens[5]
		assert.Equal(suite.T(), TokenAssign, token.Kind)
		assert.Equal(suite.T(), "=", token.Value)
	}

	tokenEOF := tokens[len(tokens)-1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexIdentifier() {
	/**
	 * Test if the lexer can tokenize an Identifier
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */

	errors := []Error{}
	tokens := suite.lexer.Lex("my_Identifier ", &errors)

	assert.Equal(suite.T(), 0, len(errors))
	assert.Equal(suite.T(), 2, len(tokens))
	token := tokens[0]

	assert.Equal(suite.T(), TokenIdentifier, token.Kind)
	assert.Equal(suite.T(), "my_Identifier", token.Value)

	tokenEOF := tokens[1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexNumber() {
	/**
	 * Test if the lexer can tokenize a Number
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */
	errors := []Error{}
	tokens := suite.lexer.Lex("123 ", &errors)

	assert.Equal(suite.T(), 0, len(errors))
	assert.Equal(suite.T(), 2, len(tokens))
	token := tokens[0]

	assert.Equal(suite.T(), TokenNumber, token.Kind)
	assert.Equal(suite.T(), "123", token.Value)

	tokenEOF := tokens[1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexKeyword() {
	/**
	 * Test if the lexer can tokenize a Keyword
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */
	errors := []Error{}
	tokens := suite.lexer.Lex("proc ", &errors)

	assert.Equal(suite.T(), 0, len(errors))
	assert.Equal(suite.T(), 2, len(tokens))
	token := tokens[0]

	assert.Equal(suite.T(), TokenProcedure, token.Kind)
	assert.Equal(suite.T(), "proc", token.Value)

	tokenEOF := tokens[1]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexMultipleTokens() {
	/**
	 * Test if the lexer can tokenize many tokens
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */
	errors := []Error{}
	tokens := suite.lexer.Lex("proc my_procedure3 ", &errors)

	assert.Equal(suite.T(), 0, len(errors))
	assert.Equal(suite.T(), 3, len(tokens))
	token0 := tokens[0]

	assert.Equal(suite.T(), TokenProcedure, token0.Kind)
	assert.Equal(suite.T(), "proc", token0.Value)

	token1 := tokens[1]
	assert.Equal(suite.T(), TokenIdentifier, token1.Kind)
	assert.Equal(suite.T(), "my_procedure3", token1.Value)

	tokenEOF := tokens[2]
	assert.Equal(suite.T(), TokenEOF, tokenEOF.Kind)
	assert.Equal(suite.T(), "", tokenEOF.Value)
}

func (suite *LexerTests) TestLexBadIdentifier() {
	/**
	 * Test if the lexer can not tokenize a bad Identifier
	 * It should add an error to the errors list
	 * It should always return a EOF token at the end
	 *  of every text read.
	 */

	errors := []Error{}
	tokens := suite.lexer.Lex("my_1&3nt1f13r ", &errors)

	assert.Equal(suite.T(), 1, len(errors))
	assert.Equal(suite.T(), 0, len(tokens))
}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func TestLexerTestsSuite(t *testing.T) {
	suite.Run(t, new(LexerTests))
}
