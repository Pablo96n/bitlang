package bitlang

import (
	"fmt"
)

/* Prints tokens in a graphically pleasant manner */
func Print(tokens []Token) {
	for _, token := range tokens {
		switch token.Kind {
		case TokenEOL:
			fmt.Printf("<%v>", token.Kind.String())
			fmt.Println()
		default:
			fmt.Printf("<%v>", token.Kind.String())
		}
	}
	fmt.Println()
}
