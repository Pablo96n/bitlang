package bitlang

import (
  "fmt"
  "testing"
  "github.com/stretchr/testify/assert"
  "github.com/stretchr/testify/suite"
)

type ErrorPrintTests struct {
  suite.Suite
}

// before all tests
func (suite *ErrorPrintTests) SetupSuite() {
  Log = &LoggerMock{}
}

// after each test
func (suite *ErrorPrintTests) TearDownTest() {
  logger := Log.(*LoggerMock)
  logger.Message = ""
}

func (suite *ErrorPrintTests) TestDummy() {
  /**
  * FIX: Este test no testea el verdadero output
  * correctamente porque no se cual debería ser
  */
  file := "import myLib\nprocedure 456proc\n{}\n"
  err := ErrorMessage("Invalid procedure name, it should be an identifier", Span{2, 3})
  PrintError("MyFile.bit", file, err)

  logger := Log.(*LoggerMock)

  assert.Equal(suite.T(), "Error: Invalid procedure name, it should be an identifier\n\x1b[0m-----", logger.Message)
}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func TestErrorPrintTestsSuite(t *testing.T) {
  suite.Run(t, new(ErrorPrintTests))
}

type LoggerMock struct {
  Logger
  Message string
}

func (log *LoggerMock) Printf(fmtStr string, v ...interface{}) {
  log.Message += fmt.Sprintf(fmtStr, v...)
}

func (log *LoggerMock) Println(fmtStr string) {
  log.Message += fmtStr
}

func (log *LoggerMock) Print(fmtStr string) {
  log.Message += fmtStr
}
