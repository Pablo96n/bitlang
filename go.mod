module bitlogic-language

go 1.18

require github.com/stretchr/testify v1.7.2

require (
	github.com/766b/go-outliner v0.0.0-20180511142203-fc6edecdadd7 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20210608161538-9736a4bde949 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	golang.org/x/tools v0.1.11 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
